<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lb_2</title>
    <script src="js/jquery-3.5.0.min.js"></script>
    <script src="js/ajax.js"></script>
</head>
<body>
    <p><b>список компьютеров с заданным типом центрального процессора</b></p>
    <select name="proc" id="">
        <option value="0">i5</option> 
		<option value="1">Celeron</option>
		<option value="2">Athlon</option>
		<option value="3">Ryzen</option>
    </select>
    <p><input type="button" value="Выполнить" onclick="getProc();"></p>
	<ul id="result1"></ul>

    <p><b>список компьютеров с установленным ПО (название ПО выбирается из перечня)</b></p>
    <select name="po" id="">
        <option value="0">Windows</option> 
		<option value="1">Linux</option>
		<option value="2">MacOsX</option>
    </select>
    <p><input type="button" value="Выполнить" onclick="getPo();"></p>
	<ul id="result2"></ul>

    <p><b>список компьютеров с истекшим гарантийным сроком(No это список пк с истекшим сроком)</b></p>
    <p><input type="button" value="Выполнить" onclick="getNo();"></p>
	<ul id="result3"></ul>
</body>
</html>