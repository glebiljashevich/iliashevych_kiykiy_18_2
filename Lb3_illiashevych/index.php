<?php require 'connection.php'; ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lb_3</title>
    <script src="js/jquery-3.5.0.min.js"></script>
    <script src="js/ajax.js"></script>
</head>
<body>
    <form id="Form1">
        <p><b>список компьютеров с заданным типом центрального процессора</b></p>
        <select name="proc">
            <option>i9</option>
            <option>i7</option>
        </select>
        <p>
            <input type="button" value="Выбрать" onclick="getPage();">
            <input type="button" value="Данные из LocalStorage" onclick="getLocal1(this);">
            <input type="button" value="Очистить" onclick="$('#result1').html('')">
        </p>
    </form>
	<div id="result1"></div>

    <form action="po.php" method="get">
        <p><b>список компьютеров с установленным ПО (название ПО выбирается из перечня)</b></p>
        <select name="po">
            <option>windows</option>
            <option>linux</option>
        </select>
        <p>
            <input type="submit">
        </p>
    </form>

    <form action="gua.php" method="get">
        <p><b>список компьютеров с истекшим гарантийным сроком(No это список пк с истекшим сроком)</b></p>
        <p>
            <input type="submit">
        </p>
    </form>

</body>
</html>